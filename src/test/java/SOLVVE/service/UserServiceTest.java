package SOLVVE.service;

import SOLVVE.domain.UserProfile;
import SOLVVE.domain.UserStatus;
import SOLVVE.domain.Uzer;
import SOLVVE.dto.user.UserCreateDTO;
import SOLVVE.dto.user.UserPatchDTO;
import SOLVVE.dto.user.UserReadDTO;
import SOLVVE.exception.EntityNotFoundException;
import SOLVVE.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = "delete from uzer", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UserServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    public List<UserProfile> createUserProfileList() {
        List<UserProfile> userProfileDumb = new ArrayList<>();
        UserProfile userProfileUnit = new UserProfile();
        userProfileUnit.setId(UUID.randomUUID());
        userProfileUnit.setUzer(new Uzer());
        userProfileUnit.setActivRating(1.23);
        userProfileUnit.setReviewRating(2.34);
        userProfileUnit.setTrustRating(3.45);
        return userProfileDumb;
    }

    public Uzer createUser() {
        Uzer user = new Uzer();
        user.setName("Joey");
        user.setEmail("789");
        user.setUserStatus(UserStatus.REGISTERED_USER);
        user.setUserProfile(createUserProfileList());
        return userRepository.save(user);
    }

    @Test
    public void testGetUser() {
        Uzer user = createUser();

        UserReadDTO readDTO = userService.getUser(user.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(user);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetUserWrongId() {
        userService.getUser(UUID.randomUUID());
    }

    @Test
    public void testCreateUser() {
        UserCreateDTO create = new UserCreateDTO();
        create.setName("Joey");
        create.setEmail("789");
        create.setUserStatus(UserStatus.REGISTERED_USER);
        create.setUserProfile(createUserProfileList());

        UserReadDTO read = userService.createUser(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Uzer user = userRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(user);
    }

    @Test
    public void testPatchUser() {
        Uzer user = createUser();

        UserPatchDTO patch = new UserPatchDTO();
        patch.setName("Ross");
        patch.setEmail("456");
        patch.setUserStatus(UserStatus.MODERATOR);
        patch.setUserProfile(createUserProfileList());

        UserReadDTO read = userService.patchUser(user.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        user = userRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchUserEmptyPatch() {
        Uzer user = createUser();

        UserPatchDTO patch = new UserPatchDTO();
        UserReadDTO read = userService.patchUser(user.getId(), patch);

        Assert.assertNotNull(read.getName());
        Assert.assertNotNull(read.getEmail());
        Assert.assertNotNull(read.getUserStatus());
        Assert.assertNotNull(read.getUserProfile());

        Uzer userAfterUpdate = userRepository.findById(read.getId()).get();

        Assert.assertNotNull(userAfterUpdate.getName());
        Assert.assertNotNull(userAfterUpdate.getEmail());
        Assert.assertNotNull(userAfterUpdate.getUserStatus());
        Assert.assertNotNull(userAfterUpdate.getUserProfile());

        Assertions.assertThat(user).isEqualToComparingFieldByField(userAfterUpdate);
    }

    @Test
    public void testDeleteUser() {
        Uzer user = createUser();

        userService.deleteUser(user.getId());
        Assert.assertFalse(userRepository.existsById(user.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteUserNotFound() {
        userService.deleteUser(UUID.randomUUID());
    }
}
