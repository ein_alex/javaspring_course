package SOLVVE.controller;

import SOLVVE.domain.Uzer;
import SOLVVE.dto.userprofile.UserProfileCreateDTO;
import SOLVVE.dto.userprofile.UserProfilePatchDTO;
import SOLVVE.dto.userprofile.UserProfileReadDTO;
import SOLVVE.service.UserProfileService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserProfileController.class)
public class UserProfileControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserProfileService userProfileService;

    @MockBean
    private Uzer userExample;

    private UserProfileReadDTO createUserProfileReadDto() {
        UserProfileReadDTO readDTO = new UserProfileReadDTO();
        readDTO.setId(UUID.randomUUID());
        readDTO.setUserId(userExample);
        readDTO.setActivRating(1.23);
        readDTO.setReviewRating(2.34);
        readDTO.setTrustRating(3.45);
        return readDTO;
    }

    @Test
    public void testGetUserProfile() throws Exception {

        UserProfileReadDTO userProfileDto = createUserProfileReadDto();

        Mockito.when(userProfileService.getUserProfile(userProfileDto.getId())).thenReturn(userProfileDto);

        String resultJson = mvc.perform(get("/api/v1/uzers/{id}", userProfileDto.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        UserProfileReadDTO actualUserProfile = objectMapper.readValue(resultJson, UserProfileReadDTO.class);
        Assertions.assertThat(actualUserProfile).isEqualToComparingFieldByField(userProfileDto);

        Mockito.verify(userProfileService).getUserProfile(userProfileDto.getId());
    }

    @Test
    public void testCreateUserProfile() throws Exception {

        UserProfileCreateDTO create = new UserProfileCreateDTO();
        create.setUserId(userExample);
        create.setActivRating(1.23);
        create.setReviewRating(2.34);
        create.setTrustRating(3.45);

        UserProfileReadDTO read = createUserProfileReadDto();

        Mockito.when(userProfileService.createUserProfile(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/uzer_profiles")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn()
                .getResponse().getContentAsString();

        UserProfileReadDTO actualUserProfile = objectMapper.readValue(resultJson, UserProfileReadDTO.class);
        Assertions.assertThat(actualUserProfile).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchUserProfile() throws Exception {
        UserProfilePatchDTO patchDTO = new UserProfilePatchDTO();
        patchDTO.setUserId(userExample);
        patchDTO.setActivRating(1.23);
        patchDTO.setReviewRating(2.34);
        patchDTO.setTrustRating(3.45);

        UserProfileReadDTO readDTO = createUserProfileReadDto();

        Mockito.when(userProfileService.patchUserProfile(readDTO.getId(), patchDTO)).thenReturn(readDTO);

        String resultJson = mvc.perform(patch("/api/v1/uzers/{id}", readDTO.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserProfileReadDTO actualUserProfile = objectMapper.readValue(resultJson, UserProfileReadDTO.class);
        Assert.assertEquals(readDTO, actualUserProfile);
    }

    @Test
    public void testDeleteUser() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/uzers/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(userProfileService).deleteUserProfile(id);
    }

}
