package SOLVVE.controller;

import SOLVVE.domain.UserProfile;
import SOLVVE.domain.UserStatus;
import SOLVVE.domain.Uzer;
import SOLVVE.dto.user.UserCreateDTO;
import SOLVVE.dto.user.UserPatchDTO;
import SOLVVE.dto.user.UserReadDTO;
import SOLVVE.exception.EntityNotFoundException;
import SOLVVE.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    //@MockBean
    //private UserProfile userProfileMock;

    public List<UserProfile> createUserProfileList() {
        List<UserProfile> userProfileDumb = new ArrayList<>();
        UserProfile userProfileUnit = new UserProfile();
        userProfileUnit.setId(UUID.randomUUID());
        userProfileUnit.setUzer(new Uzer());
        userProfileUnit.setActivRating(1.23);
        userProfileUnit.setReviewRating(2.34);
        userProfileUnit.setTrustRating(3.45);
        return userProfileDumb;
    }

    public UserReadDTO createUserReadDto() {
        UserReadDTO readDTO = new UserReadDTO();
        readDTO.setId(UUID.randomUUID());
        readDTO.setName("Joey");
        readDTO.setEmail("789");
        readDTO.setUserProfile(createUserProfileList());
        readDTO.setUserStatus(UserStatus.REGISTERED_USER);
        return readDTO;
    }

    @Test
    public void testGetUser() throws Exception {

        UserReadDTO userDto = createUserReadDto();

        Mockito.when(userService.getUser(userDto.getId())).thenReturn(userDto);

        String resultJson = mvc.perform(get("/api/v1/uzer/{id}", userDto.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);
        UserReadDTO actualUser = objectMapper.readValue(resultJson, UserReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(userDto);

        Mockito.verify(userService).getUser(userDto.getId());
    }

    @Test
    public void testGetUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Uzer.class, wrongId);
        Mockito.when(userService.getUser(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/uzer/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateUser() throws Exception {

        UserCreateDTO create = new UserCreateDTO();
        create.setName("Joey");
        create.setEmail("789");
        create.setUserProfile(createUserProfileList());
        create.setUserStatus(UserStatus.REGISTERED_USER);

        UserReadDTO read = createUserReadDto();

        Mockito.when(userService.createUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/uzer")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn()
                .getResponse().getContentAsString();

        UserReadDTO actualUser = objectMapper.readValue(resultJson, UserReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchUser() throws Exception {

        UserPatchDTO patchDTO = new UserPatchDTO();
        patchDTO.setName("Joey");
        patchDTO.setEmail("789");
        patchDTO.setUserProfile(createUserProfileList());
        patchDTO.setUserStatus(UserStatus.REGISTERED_USER);

        UserReadDTO readDTO = createUserReadDto();

        Mockito.when(userService.patchUser(readDTO.getId(), patchDTO)).thenReturn(readDTO);

        String resultJson = mvc.perform(patch("/api/v1/uzer/{id}", readDTO.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        UserReadDTO actualUser = objectMapper.readValue(resultJson, UserReadDTO.class);
        Assert.assertEquals(readDTO, actualUser);
    }

    @Test
    public void testDeleteUser() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/uzer/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(userService).deleteUser(id);
    }

}
