package SOLVVE.domain;

public enum UserStatus {
    VISITOR, REGISTERED_USER, CONTENT_MANAGER, MODERATOR, ADMIN
}
