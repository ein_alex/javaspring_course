//package SOLVVE.domain;
//
//import lombok.Getter;
//import lombok.Setter;
//
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.Id;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import java.util.UUID;
//
//@Getter
//@Setter
//@Entity
//public class Film {
//
//    @Id
//    @GeneratedValue
//    private UUID id;
//    private String title;
//    private Date releaseDate;
//    private String descr;
//    private double userRating;
//    private List<String> genre;
//    private ArrayList<Crew> crew;
//}
