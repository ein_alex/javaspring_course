package SOLVVE.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
//Composite
public class Uzer {

    @Id
    @GeneratedValue
    private UUID id;
    private String name;
    private String email;

    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;

    @OneToMany(mappedBy = "uzer")
    private List<UserProfile> userProfile;
}