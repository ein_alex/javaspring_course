package SOLVVE.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Setter
@Getter
@Entity
//Composite item (Uzer)
public class UserProfile {

    @Id
    @GeneratedValue
    private UUID id;
    private Double activRating;
    private Double reviewRating;
    private Double trustRating;
    //private ArrayList<Like> likesList;

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private Uzer uzer;

}
