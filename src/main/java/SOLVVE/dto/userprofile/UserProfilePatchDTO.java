package SOLVVE.dto.userprofile;

import SOLVVE.domain.Uzer;
import lombok.Data;

@Data
public class UserProfilePatchDTO {

    private Uzer userId;
    private Double activRating;
    private Double reviewRating;
    private Double trustRating;
}
