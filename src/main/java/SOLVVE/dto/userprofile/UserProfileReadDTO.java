package SOLVVE.dto.userprofile;

import SOLVVE.domain.Uzer;
import lombok.Data;

import java.util.UUID;

@Data
public class UserProfileReadDTO {

    private UUID id;
    private Uzer userId;
    private Double activRating;
    private Double reviewRating;
    private Double trustRating;
    //private ArrayList<Like> likesList;

}
