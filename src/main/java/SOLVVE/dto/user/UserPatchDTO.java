package SOLVVE.dto.user;

import SOLVVE.domain.UserProfile;
import SOLVVE.domain.UserStatus;
import lombok.Data;

import java.util.List;

@Data
public class UserPatchDTO {

    private String name;
    private String email;
    private List<UserProfile> userProfile;
    private UserStatus userStatus;
}
