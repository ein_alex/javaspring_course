package SOLVVE.dto.user;

import SOLVVE.domain.UserProfile;
import SOLVVE.domain.UserStatus;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class UserReadDTO {

    private UUID id;
    private String name;
    private String email;
    private List<UserProfile> userProfile;
    private UserStatus userStatus;
}
