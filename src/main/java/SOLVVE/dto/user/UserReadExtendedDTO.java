package SOLVVE.dto.user;

import SOLVVE.dto.userprofile.UserProfileReadDTO;
import lombok.Data;

import java.util.UUID;

@Data
public class UserReadExtendedDTO {

    private UUID id;

    private UserReadDTO user;
    private UserProfileReadDTO userProfile;

}
