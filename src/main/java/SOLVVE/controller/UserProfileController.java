package SOLVVE.controller;

import SOLVVE.dto.userprofile.UserProfileCreateDTO;
import SOLVVE.dto.userprofile.UserProfilePatchDTO;
import SOLVVE.dto.userprofile.UserProfileReadDTO;
import SOLVVE.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/v1/uzer_profiles")
public class UserProfileController {

    @Autowired
    private UserProfileService userProfileService;

    @GetMapping("/{id}")
    public UserProfileReadDTO getUserProfile(@PathVariable UUID id) {
        return userProfileService.getUserProfile(id);
    }

    @PostMapping
    public UserProfileReadDTO createUserProfile(@RequestBody UserProfileCreateDTO createDTO) {
        return userProfileService.createUserProfile(createDTO);
    }

    @PatchMapping("/{id}")
    public UserProfileReadDTO patchUserProfile(@PathVariable UUID id, @RequestBody UserProfilePatchDTO patch) {
        return userProfileService.patchUserProfile(id, patch);
    }

    @DeleteMapping("/{id}")
    public void deleteUserProfile(@PathVariable UUID id) {
        userProfileService.deleteUserProfile(id);
    }

}
