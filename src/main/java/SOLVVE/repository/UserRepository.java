package SOLVVE.repository;

import SOLVVE.domain.Uzer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<Uzer, UUID> {
}
