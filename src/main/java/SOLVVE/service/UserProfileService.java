package SOLVVE.service;

import SOLVVE.domain.UserProfile;
import SOLVVE.domain.Uzer;
import SOLVVE.dto.userprofile.UserProfileCreateDTO;
import SOLVVE.dto.userprofile.UserProfilePatchDTO;
import SOLVVE.dto.userprofile.UserProfileReadDTO;
import SOLVVE.exception.EntityNotFoundException;
import SOLVVE.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserProfileService {

    @Autowired
    private UserProfileRepository userProfileRepository;

    private UserProfile getUserProfileRequired(UUID id) {
        return userProfileRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException(Uzer.class, id);
        });
    }

    public UserProfileReadDTO getUserProfile(UUID id) {
        UserProfile userProfileSmpl = getUserProfileRequired(id);
        return toRead(userProfileSmpl);
    }

    private UserProfileReadDTO toRead(UserProfile userProfileSmpl) {
        UserProfileReadDTO dto = new UserProfileReadDTO();
        dto.setId(userProfileSmpl.getId());
        dto.setUserId(userProfileSmpl.getUzer());
        dto.setActivRating(userProfileSmpl.getActivRating());
        dto.setReviewRating(userProfileSmpl.getReviewRating());
        dto.setTrustRating(userProfileSmpl.getTrustRating());
        return dto;
    }

    public UserProfileReadDTO createUserProfile(UserProfileCreateDTO create) {
        UserProfile userProfile = new UserProfile();
        userProfile.setUzer(create.getUserId());
        userProfile.setActivRating(create.getActivRating());
        userProfile.setReviewRating(create.getReviewRating());
        userProfile.setTrustRating(create.getTrustRating());


        userProfile = userProfileRepository.save(userProfile);
        return toRead(userProfile);
    }

    public UserProfileReadDTO patchUserProfile(UUID id, UserProfilePatchDTO patch) {
        UserProfile userProfile = getUserProfileRequired(id);

        if (patch.getUserId() != null) {
            userProfile.setUzer(patch.getUserId());
        }

        if (patch.getActivRating() != null) {
            userProfile.setActivRating(patch.getActivRating());
        }

        if (patch.getReviewRating() != null) {
            userProfile.setReviewRating(patch.getReviewRating());
        }

        if (patch.getTrustRating() != null) {
            userProfile.setTrustRating(patch.getTrustRating());
        }

        userProfile = userProfileRepository.save(userProfile);
        return toRead(userProfile);
    }

    public void deleteUserProfile(UUID id) {
        userProfileRepository.delete(getUserProfileRequired(id));
    }

}
