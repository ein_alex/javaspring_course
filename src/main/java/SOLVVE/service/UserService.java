package SOLVVE.service;

import SOLVVE.domain.Uzer;
import SOLVVE.dto.user.UserCreateDTO;
import SOLVVE.dto.user.UserPatchDTO;
import SOLVVE.dto.user.UserReadDTO;
import SOLVVE.exception.EntityNotFoundException;
import SOLVVE.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    private Uzer getUserRequired(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> {
            throw new EntityNotFoundException(Uzer.class, id);
        });
    }

    public UserReadDTO getUser(UUID id) {
        Uzer userSmpl = getUserRequired(id);
        return toRead(userSmpl);
    }

    private UserReadDTO toRead(Uzer userSmpl) {
        UserReadDTO dto = new UserReadDTO();
        dto.setId(userSmpl.getId());
        dto.setName(userSmpl.getName());
        dto.setEmail(userSmpl.getEmail());
        dto.setUserProfile(userSmpl.getUserProfile());
        dto.setUserStatus(userSmpl.getUserStatus());
        return dto;
    }

    public UserReadDTO createUser(UserCreateDTO create) {
        Uzer user = new Uzer();
        user.setEmail(create.getEmail());
        user.setName(create.getName());
        user.setUserProfile(create.getUserProfile());
        user.setUserStatus(create.getUserStatus());

        user = userRepository.save(user);
        return toRead(user);
    }

    public UserReadDTO patchUser(UUID id, UserPatchDTO patch) {
        Uzer user = getUserRequired(id);

        if (patch.getName() != null) {
            user.setName(patch.getName());
        }

        if (patch.getEmail() != null) {
            user.setEmail(patch.getEmail());
        }

        if (patch.getUserProfile() != null) {
            user.setUserProfile(patch.getUserProfile());
        }

        if (patch.getUserStatus() != null) {
            user.setUserStatus(patch.getUserStatus());
        }

        user = userRepository.save(user);
        return toRead(user);
    }

    public void deleteUser(UUID id) {
        userRepository.delete(getUserRequired(id));
    }
}
