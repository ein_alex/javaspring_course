package SOLVVE.exception.handler;

import org.springframework.http.HttpStatus;

public class ErrorInfo {
    private final HttpStatus httpStatus;
    private final Class exceptionClass;
    private final String message;

    ErrorInfo(HttpStatus httpStatus, Class exceptionClass, String message) {
        this.httpStatus = httpStatus;
        this.exceptionClass = exceptionClass;
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public Class getExceptionClass() {
        return exceptionClass;
    }

    public String getMessage() {
        return message;
    }
}
